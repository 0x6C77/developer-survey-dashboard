## Developer survey dashboard
Dashboard visualising data from HackerRank Developer Survey 2018

**[Live demo](https://0x6c77.gitlab.io/developer-survey-dashboard)**

HackerRank Developer survey result data from:
https://www.kaggle.com/hackerrank/developer-survey-2018

GeoJSON data is a modified version of:
https://geojson-maps.ash.ms/

Icons from FontAwesome:
https://fontawesome.com


### TODO
* Currently blips on map are randomly positioned. Should randomly appear within the country of each entry in chronological order
* Remember settings and data when returning to homepage from country page
* Implement globe and map panning and zooming
* Add labelling of charts and date range
* Improve date range UX by labelling start and end dates