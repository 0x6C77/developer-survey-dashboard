"""
Filter indexes in CSV file. Takes a CSV file as input with CSV string of indexes to be preserved.
"""

import sys
import os
import argparse
import csv

parser = argparse.ArgumentParser(
    epilog='python minify.py -k "2, 3, 5, 6, 7, 96, 97, 98, 99, 100, 101, 102, 103" -i results.csv'
)
parser.add_argument("-i", "--input", help="Input CSV file")
parser.add_argument("-k", "--keep", help="Indexes to keep")
args = parser.parse_args()

data = []

# Validate input file
input_file = args.input
if not os.path.isfile(input_file):
    print "Input file doesn't exist"
    sys.exit(0)

input_file_parts = os.path.basename(input_file).split(os.extsep, 1)
output_file = "{}.min.{}".format(input_file_parts[0], input_file_parts[1])

# Parse user input to array of indexes which are to preserved
keep = []
try:
    indexes = args.keep.split(',')
    for index in indexes:
        keep.append(int(index))
except Exception:
    print "Error validating target indexes"
    sys.exit(0)

# Itterate each line of file preserving only required indexes
with open(input_file, 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
        data.append([row[i] for i in keep])

# Output modified rows
with open(output_file, 'wb') as f:
    writer = csv.writer(f)
    writer.writerows(data)