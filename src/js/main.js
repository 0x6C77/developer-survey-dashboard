import React from "react";
import { HashRouter, Route, Switch } from 'react-router-dom';
import ReactDOM from "react-dom";
import Home from "./components/Home.js";
import Country from "./components/Country.js";

ReactDOM.render((
    <HashRouter>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/country/:country" component={Country} />
        </Switch>
    </HashRouter>
    ), document.getElementById("App")
);