import React, { Component } from "react";
import Map from "./Map.js";
import DateRange from "./DateRange.js";

class Home extends Component{
    constructor(props) {
        super();

        this.history = props.history;
    }

    render(){
        return(
            <div className="view view--home">
                <div className="instructions">
                    <h1>Developer survey dashboard</h1>
                    <p>Visulisation of the HackerRank developer suvery 2018.</p>
                    <p>
                        Selecting dates in date range at the bottom will filter entries by submission date.<br/>
                        Flat map view can be toggled in top right.<br/>
                        Click a country for more detailed statistics.
                    </p>
                    <p><a href="https://gitlab.com/0x6C77/developer-survey-dashboard" target="_blank">Attributions, data sources and code</a></p>
                </div>
                <Map {...this.history} />
                <DateRange />
            </div>
        );
    }
}

export default Home;