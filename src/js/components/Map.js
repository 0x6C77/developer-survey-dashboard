import React, { Component } from "react";
import dataService from "../services/dataService.js";

class Map extends React.Component {
    constructor(history) {
        super();

        this.history = history;

        this.state = {
            data: [],
            worldTopo: [],
            width: document.body.clientWidth,
            height: document.body.clientHeight,
            tooltip: null,
            projectionAngle: 0,
            entryDots: []
        }

        Promise.all(
            [
                this._getMapData(),
                dataService.getParticipatingCountries()
            ]
        ).then((values) => {
            this.mapData = values[0];
            this.setState({
                worldTopo: this._combineData(this.mapData, values[1]),
                projectionGlobe: true
            });

            this.spinningGlobeOffset = 0;
            this._startGlobeAnimation();
            this._startAnimation();
        });


        dataService.onFilterChange(() => {
            dataService.getParticipatingCountries().then((values) => {
                this.setState({
                    worldTopo: this._combineData(this.mapData, values)
                });
            });
        });


        // Watch for browser resize
        window.addEventListener("resize", () => {
            this.setState({
                width: document.body.clientWidth,
                height: document.body.clientHeight
            });
        });

        this.geoPath = d3.geoPath();

        this._toggleGlobe = this._toggleGlobe.bind(this);
        this._handleCountryMouseEnter = this._handleCountryMouseEnter.bind(this);
        this._handleCountryMouseLeave = this._handleCountryMouseLeave.bind(this);
        this._handleCountryMouseClick = this._handleCountryMouseClick.bind(this);
        this._handleGlobeMouseEnter = this._handleGlobeMouseEnter.bind(this);
        this._handleGlobeMouseLeave = this._handleGlobeMouseLeave.bind(this);
        this._startGlobeAnimation = this._startGlobeAnimation.bind(this);
        this._stopGlobeAnimation = this._stopGlobeAnimation.bind(this);
    }

    _getMapData() {
        return new Promise((resolve, reject) => {
            d3.json("data/map/world.geo.json", function(error, world) {
                resolve(world.features);
            });
        });
    }

    _projection(globe) {
        if (globe) {
            return d3.geoOrthographic()
                .translate([(this.state.width / 2), this.state.height / 2])
                .scale( this.state.height * 0.3 )
                .clipAngle(90)
                .precision(0.3)
                .rotate([ this.state.projectionAngle, 0, 0 ]);
        } else {
            return d3.geoMercator()
                .translate([this.state.width / 2, this.state.height / 2])
                .scale( this.state.height / 2 / Math.PI);
        }
    }

    _toggleGlobe() {
        var globe = !this.state.projectionGlobe;

        if (globe) {
            this._startGlobeAnimation();
        } else {
            this._stopGlobeAnimation();
        }

        this.setState({
            projectionGlobe: globe
        });
    }

    _startAnimation() {
        this.spinningGlobeInterval = d3.timer(() => {
            var entryDots = this.state.entryDots;

            if (!this.nextNewEntryDot || this.nextNewEntryDot < Date.now()) {
                entryDots.push({
                    added: Date.now(),
                    coordinates: [Math.random()*360 - 180, Math.random()*360 - 180]
                });

                entryDots = _.filter(entryDots, (dot) => {
                    return (Date.now() - dot.added) < 5000
                });

                // Show next entry point in 1 second
                this.nextNewEntryDot = new Date();
                this.nextNewEntryDot.setSeconds(this.nextNewEntryDot.getSeconds() + 1);
            }

            var angle = this.state.projectionAngle;
            if (this.globeSpinng) {
                angle = this.spinningGlobeOffset + (-0.01 * (Date.now() - this.spinningGlobeStart));
            }

            this.setState({
                projectionAngle: angle,
                entryDots: entryDots
            });
        });
    }

    _stopAnimation() {
        this.spinningGlobeInterval.stop();
    }

    _startGlobeAnimation() {
        this.spinningGlobeStart = Date.now();
        this.globeSpinng = true;
    }

    _stopGlobeAnimation() {
        this.globeSpinng = false;
        this.spinningGlobeOffset = this.state.projectionAngle;
    }

    _combineData(worldTopo, participants) {
        return worldTopo.map((c, index) => {
            var country = c.properties.iso.toLowerCase();
            if (participants &&
                participants.countries[country]) {
                    c.properties.participants = participants.countries[country];
                    c.properties.fill = this._getFillColour(
                        c.properties.participants,
                        participants.max
                    );
            } else {
                c.properties.participants = 0;
                c.properties.fill = null;
            }
            return c;
        });
    }

    _getFillColour(target, max) {
        var percentage = target / max;
        return colorMixer([102,164,254], [49,82,132], percentage);
    }

    _handleCountryMouseEnter(event, item) {
        this._stopGlobeAnimation();

        var target = event.target;
        if (target.classList.contains('country--deactive')) {
            return;
        }

        var tooltip = {};

        tooltip.country = item.name;
        tooltip.participants = item.participants
        tooltip.flag = 'https://flagpedia.net/data/flags/h80/' + item.iso.toLowerCase() + '.png';

        // Get target position of tooltip
        var bbox = target.getBBox();
        tooltip.left = bbox.x + bbox.width + 10,
        tooltip.top = bbox.y + (bbox.height / 2) - 20;

        this.setState({
            tooltip: tooltip
        });
    }

    _handleCountryMouseLeave() {
        this.globeSpinng = true;

        this.setState({
            tooltip: null
        });
    }

    _handleCountryMouseClick(event, item) {
        var target = event.target;
        if (target.classList.contains('country--deactive')) {
            return;
        }

        this.history.push('/country/' + item.iso.toLowerCase());
    }

    _handleGlobeMouseEnter(event, item) {
        this._stopGlobeAnimation();
    }

    _handleGlobeMouseLeave(event, item) {
        this._startGlobeAnimation();
    }

    componentWillUnmount() {
        this._stopAnimation();
    }

    render() {
        var projection = this._projection(this.state.projectionGlobe);
        this.geoPath.projection(projection);

        return (
            <div className='map'>
                <div className="map-projection-toggle" onClick={ this._toggleGlobe } >
                    <i className={ "fas fa-globe-americas " + ( this.state.projectionGlobe ? 'active' : '' ) }  />
                    <i className={ "fas fa-map " + ( !this.state.projectionGlobe ? 'active' : '' ) }/>
                </div>

                <svg>
                    {
                        this.state.projectionGlobe ?
                            <circle className="background"
                                cx={ this.state.width / 2}
                                cy={ this.state.height / 2}
                                r={ projection.scale() }
                                onMouseEnter={ this._handleGlobeMouseEnter }
                                onMouseLeave={ this._handleGlobeMouseLeave }
                            />
                        : null
                    }

                    <g className="countries">
                        {
                            this.state.worldTopo.map((country, index) => (
                                <path
                                    className={ country.properties.participants ? 'country' : 'country country--deactive' }
                                    key={ `path-${ index }` }
                                    d={ this.geoPath(country) }
                                    fill={ country.properties.fill }
                                    onMouseEnter={ (event) => { this._handleCountryMouseEnter(event, country.properties) } }
                                    onMouseLeave={ this._handleCountryMouseLeave }
                                    onClick={ (event) => { this._handleCountryMouseClick(event, country.properties) } }
                                />
                            ))
                        }
                    </g>

                    <g className="entries">
                        {
                            this.state.entryDots.map((dot) => {
                                let point = {
                                        type: "Point",
                                        coordinates: dot.coordinates,
                                    },
                                    radius = (Date.now() - dot.added) * 0.003,
                                    opacity = 1 - (Date.now() - dot.added) / 5000;

                                this.geoPath.pointRadius(radius);
                                let path = this.geoPath(point);

                                return (
                                    <path
                                        key={ dot.added }
                                        d={ path }
                                        fill="#23ae33"
                                        fillOpacity={ opacity }
                                    />
                                )
                            })
                        }
                    </g>

                </svg>
                {
                    this.state.tooltip ?
                        <div className="map-tooltip" style={{ top: this.state.tooltip.top, left: this.state.tooltip.left }}>
                            <div className="map-tooltip-flag" style={{ backgroundImage: 'url(' + this.state.tooltip.flag + ')' }}></div>
                            <h1>{ this.state.tooltip.country }</h1>
                            <p>
                                { this.state.tooltip.participants.toLocaleString('en') } participant{ this.state.tooltip.participants == 1 ? '' : 's' }
                            </p>
                            <p className="map-tooltip-cta">Click for more statistics</p>
                        </div>
                    : null
                }
            </div>
        );
    }
}

export default Map;






// https://stackoverflow.com/a/32171077/794119
//colorChannelA and colorChannelB are ints ranging from 0 to 255
function colorChannelMixer(colorChannelA, colorChannelB, amountToMix){
    var channelA = colorChannelA*amountToMix;
    var channelB = colorChannelB*(1-amountToMix);
    return parseInt(channelA+channelB);
}
//rgbA and rgbB are arrays, amountToMix ranges from 0.0 to 1.0
//example (red): rgbA = [255,0,0]
function colorMixer(rgbA, rgbB, amountToMix){
    var r = colorChannelMixer(rgbA[0],rgbB[0],amountToMix);
    var g = colorChannelMixer(rgbA[1],rgbB[1],amountToMix);
    var b = colorChannelMixer(rgbA[2],rgbB[2],amountToMix);
    return "rgb("+r+","+g+","+b+")";
}