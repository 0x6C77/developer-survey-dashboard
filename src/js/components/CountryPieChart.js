import React, { Component } from "react";

class CountryPieChart extends Component{
    constructor(props) {
        super();

        var colours = [
            '#6ea2cf',
            '#cb8acb',
            '#75b675',
            '#7e7e7e'
        ]

        this.title = props.title;
        this.data = [];

        var total = 0;
        _.forEach(props.data, (value) => {
            total += value;
        });

        var index = 0;
        _.forEach(props.data, (count, label) => {
            let section = {};
            section.label = label;
            section.value = count;
            section.percentage = Math.round((count / total) * 100);

            section.colour = colours[index];

            this.data.push(section);
            index++;
        });

        this.state = {
            hover: null,
            width: 0
        }

        this._handleMouseOver = this._handleMouseOver.bind(this);
        this._handleMouseLeave = this._handleMouseLeave.bind(this);
    }

    componentDidMount() {
        const width = document.getElementsByClassName('view--country')[0].offsetWidth * 0.4;
        this.setState({ width });
    }

    _handleMouseOver(label) {
        this.setState({ hover: label });
    }

    _handleMouseLeave() {
        this.setState({ hover: null });
    }

    render() {
        if (!this.state.width) {
            return null;
        }

        let pie = d3.pie().value(function(d) { return d.value; });

        var width = this.state.width,
            height = width,
            radius = (width * 0.9) / 2,
            title = this.title;

        return (
            <div className="chart chart--pie">
                <svg width={ width } height={ height }>
                    <g transform={`translate(${width/2}, ${height/2})`}>
                        {
                            pie(_.values(this.data)).map((value, index) => {
                                let arc = d3.arc()
                                    .innerRadius(radius * 0.8)
                                    .outerRadius(radius * 0.95)
                                    .cornerRadius(5)
                                    .padAngle(0.02);

                                if (value.data.label == this.state.hover) {
                                    arc.innerRadius(radius * 0.75)
                                        .outerRadius(radius);

                                    title = `${this.state.hover} ${value.data.percentage}%`
                                }

                                return (
                                    <path
                                        key={ index }
                                        d={ arc(value) }
                                        fill={ value.data.colour }
                                        onMouseEnter={ () => { this._handleMouseOver(value.data.label) } }
                                        onMouseLeave={ this._handleMouseLeave }
                                    />
                                )
                            })
                        }
                        <text className="title">
                            { title }
                        </text>
                    </g>
                </svg>
            </div>
        );
    }
}

export default CountryPieChart;