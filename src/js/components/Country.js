import React, { Component } from "react";
import dataService from "../services/dataService.js";
import CountryPieChart from "./CountryPieChart.js";
import CountryAgeLanguageGraph from "./CountryAgeLanguageGraph.js";

class Country extends Component{
    constructor(props) {
        super();

        this.history = props.history;
        this.countryISO = props.match.params.country;

        this.state = {
            country: null
        }

        dataService.getCountry(this.countryISO).then((data) => {
            this.setState({
                country: data
            });
        });
    }

    render() {
        if (!this.state.country) {
            return 'Loading';
        }

        return(
            <div className="view view--country">
                <a href="#" onClick={() => { this.history.push('/') }}>Back to map</a>

                <div className="country-name">
                    <div className="country-flag" style={{ backgroundImage: 'url(' + this.state.country.flag + ')' }}></div>
                    <h1>{ this.state.country.name }</h1>
                </div>

                <ul className="country-stats">
                    <li>
                        <i className="fas fa-pencil-alt"/>
                        Participants: { this.state.country.participants.toLocaleString('en') }
                    </li>
                    <li>
                        <i className="fas fa-users"/>
                        Population: { this.state.country.population.toLocaleString('en') }
                    </li>

                    {
                        this.state.country.gini ? (
                            <li>
                                Income inequality
                                <div className="country-bar-chart">
                                    <div className="country-bar-chart-fill" style={{ width: (100 - this.state.country.gini) + '%' }}></div>
                                </div>
                                <div className="country-bar-note">
                                    Gini Index - Income disparity (lower is better equality)
                                </div>
                            </li>
                        ) : ''
                    }
                </ul>

                <div className="country-charts">
                    <CountryPieChart title="Gender" data={this.state.country.genders} />
                    <CountryPieChart title="Education" data={this.state.country.education} />
                </div>
                
                <CountryAgeLanguageGraph data={this.state.country.languages} />
            </div>
        );
    }
}

export default Country;