import React, { Component } from "react";
import dataService from "../services/dataService.js";

class DateRange extends React.Component {
    constructor() {
        super();

        this.state = {
            dates: []
        }

        this.dateRange = null;

        // Load submission ranges
        dataService.getSubmissionRange().then((dates) => {
            let max = _.maxBy(dates, 'count').count;

            this.setState({
                dates: dates,
                max: max
            });
        });

        this._handleClick = this._handleClick.bind(this);
    }

    _getActiveDates() {
        var dates = _.map(this.state.dates, (value) => {
            if (!this.dateRange) {
                value.deactive = false;
            } else  if (this.dateRange.length == 1) {
                value.deactive = !(value.date == this.dateRange[0]);
            } else  if (this.dateRange.length == 2) {
                value.deactive = !(value.date >= this.dateRange[0] && value.date <= this.dateRange[1]);
            }

            return value;
        });

        let max = _.maxBy(dates, (item) => {
            return item.deactive ? 0 : item.count;
        }).count;

        this.setState({
            dates: dates,
            max: max
        });
    }

    _getSegmentHeight(item) {
        var minHeight = 30,
            maxHeight = document.body.clientHeight * 0.2;

        if (item.deactive) {
            return minHeight;
        } else {
            return minHeight +_(item.count / this.state.max) * (maxHeight - minHeight);
        }
    }

    _handleClick(item) {
        if (this.dateRange && this.dateRange.length == 1 && this.dateRange[0] < item.date) {
            this.dateRange.push(item.date);
        } else {
            this.dateRange = [item.date];
        }

        dataService.setDateRange(this.dateRange);
        this._getActiveDates();
    }

    render() {
        return (
            <div className="dateRange">
                {
                    this.state.dates.map((item, id) => (
                        <div
                            className={ item.deactive ? 'deactive' : '' }
                            style={{height: this._getSegmentHeight(item) + 'px'}}
                            key={ `path-${ id }` }
                            onClick={ e => this._handleClick(item) }
                        >
                            <div>{ item.label }</div>
                        </div>
                    ))
                }
            </div>
        )
    }

}

export default DateRange;
