import React, { Component } from "react";

// Adapted from: https://bl.ocks.org/mbostock/3887051
class CountryAgeLanguageGraph extends Component{
    constructor(props) {
        super();
    }

    componentDidMount() {
        var svgWidth = document.getElementsByClassName('view--country')[0].offsetWidth,
            svgHeight = 300,
            padding = 30;

        var keys = [
            '< 12',
            '12 - 18',
            '18 - 24',
            '25 - 34',
            '35 - 44',
            '45 - 54',
            '55 - 64',
            '65 - 74',
            '75+'
        ];
        var height = svgHeight - (padding * 2),
            width = svgWidth - (padding * 2),
            x1 = d3.scaleBand().padding(0.05),
            x0 = d3.scaleBand()
                    .rangeRound([0, width])
                    .paddingInner(0.1),
            y = d3.scaleLinear().rangeRound([height, 0]),
            z = d3.scaleOrdinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

        x0.domain(this.props.data.map(function(a) { return a.language }));
        x1.domain(keys).rangeRound([0, x0.bandwidth()]);
        y.domain([0, d3.max(this.props.data, function(d) { return d3.max(keys, function(key, index) { return d.values[index]; }); })]).nice();

        var svg = d3.select(this.refs.svg);

        var g = svg.append("g");

        g.attr("transform", `translate(${padding}, ${padding})`);

        g.append("g")
            .selectAll("g")
            .data(this.props.data)
            .enter().append("g")
            .attr("transform", function(d) { return "translate(" + x0(d.language) + ",0)"; })
            .selectAll("rect")
            .data(function(d) { return keys.map(function(key, index) { return {key: key, value: d.values[index]}; }); })
            .enter().append("rect")
            .attr("x", function(d) { return x1(d.key); })
            .attr("y", function(d) { return y(d.value); })
            .attr("width", x1.bandwidth())
            .attr("height", function(d) { return height - y(d.value); })
            .attr("fill", function(d) { return z(d.key); });


        g.append("g")
            .attr("class", "axis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x0));

        g.append("g")
            .attr("class", "axis")
            .call(d3.axisLeft(y).ticks(null, "s"));

        var legend = g.append("g")
            .attr("font-size", 10)
            .attr("text-anchor", "end")
            .selectAll("g")
            .data(keys)
            .enter().append("g")
            .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        legend.append("rect")
            .attr("x", width - 15)
            .attr("width", 15)
            .attr("height", 15)
            .attr("fill", z);

        legend.append("text")
            .attr("x", width - 24)
            .attr("y", 9.5)
            .attr("dy", "0.32em")
            .text(function(d) { return d; });
    }

    render() {
        return (
            <div className="country-language-graph">
                <h3>Language proficiency grouped by age</h3>
                <svg ref="svg" width="100%" height="300"></svg>
            </div>
        )
    }
}

export default CountryAgeLanguageGraph;