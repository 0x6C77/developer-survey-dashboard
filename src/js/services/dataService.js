import hackerRankDataService from "./hackerRankDataService.js";

let dataService = {
    hackerRank: new hackerRankDataService(),

    rangeObservers: [],
    setDateRange: (range) => {
        dataService.dateRange = range;

        _.forEach(dataService.rangeObservers, (callback) => {
            callback();
        });
    },

    onFilterChange: (callback) => {
        dataService.rangeObservers.push(callback)
    },

    getSubmissionRange: () => {
        return new Promise((resolve, reject) => {
            dataService.hackerRank.getSubmissionRange().then((data) => {
                data = _.sortBy(data, 'date');
                resolve(data);
            });
        });
    },

    getParticipatingCountries: () => {
        return new Promise((resolve, reject) => {
            dataService.hackerRank.getParticipatingCountries(dataService.dateRange).then((data) => {
                let max = 0;

                _.forEach(data, (count, country) => {
                    if (country && count > max) {
                        max = count;
                    }
                });

                resolve({ countries: data, max });
            })
        });
    },

    getCountry: (countryISO) => {
        countryISO = countryISO.toLowerCase();

        return new Promise((resolve, reject) => {
            Promise.all(
                [
                    dataService.getCountryDetails(countryISO),
                    dataService.hackerRank.getEntriesByCountry(countryISO)
                ]
            ).then((values) => {
                var country = values[0];
                country.iso = countryISO;

                // Extract statistics
                var entries = values[1];
                country.participants = entries.length;

                country.genders = _.countBy(entries, 'gender');
                country.education = _.countBy(entries, 'education');

                // Build age/language array
                var languages = {};
                _.each(entries, (entry) => {
                    _.each(entry.languages, (language) => {
                        if (!languages[language]) {
                            languages[language] = {};
                        }

                        if (!languages[language][entry.age]) {
                            languages[language][entry.age] = 0;
                        }

                        languages[language][entry.age]++;
                    });
                });

                country.languages = [];
                _.each(languages, (row, key) => {
                    let item = {};
                    item.language = key;

                    item.values = [];
                    for (var i = 0; i <= 8; i++) {
                        item.values.push(row[i] ? row[i] : 0)
                    }

                    country.languages.push(item);
                });

                resolve(country);
            });
        });
    },

    getCountryDetails: (countryISO) => {
        return new Promise((resolve, reject) => {
            var url = `https://restcountries.eu/rest/v2/alpha/${countryISO}?fields=flag;name;nativeName;population;gini`;

            var request = new XMLHttpRequest();
            request.open('GET', url, true);

            request.onload = function() {
                if (request.status >= 200 && request.status < 400) {
                    resolve(JSON.parse(request.responseText));
                }
            };

            request.send();
        });
    }
};

export default dataService;