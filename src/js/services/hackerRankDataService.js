class hackerRankDataService {
    constructor() {
        d3.csv("data/hackerrank/country-codes.csv", (data) => {
            this.countryCodes = data;
        });
    }

    getData() {
        return new Promise((resolve, reject) => {
            if (this.entries) {
                resolve(this.entries);
            }

            var languageMap = {
                'q22LangProfC': 'C',
                'q22LangProfCPlusPlus': 'C++',
                'q22LangProfCSharp': 'C#',
                'q22LangProfGo': 'Go',
                'q22LangProfJava': 'Java',
                'q22LangProfJavascript': 'Javascript',
                'q22LangProfPython': 'Python',
                'q22LangProfRuby': 'Ruby'
            };
            d3.csv("data/hackerrank/results.min.csv", (entries) => {
                // Unify data
                entries = _.map(entries, (row) => {
                    let entry = {};
                    entry.submitted = row.EndDate;
                    entry.country = this.lookupCountryCode(row.CountryNumeric2);
                    entry.age = row.q2Age;

                    switch(row.q3Gender) {
                        case "1": entry.gender = 'male'; break;
                        case "2": entry.gender = 'female'; break;
                        default: entry.gender = 'other';
                    }

                    // Group users education
                    switch(row.q4Education) {
                        case "1":
                        case "2": entry.education = 'school'; break;
                        case "3":
                        case "4":
                        case "5": entry.education = 'college'; break;
                        case "6":
                        case "7": entry.education = 'higher'; break;
                        default: entry.education = 'other';
                    }


                    // Get proficient languages
                    entry.languages = [];
                    _.each(languageMap, (value, key) => {
                        if (row[key] == "1") {
                            entry.languages.push(value);
                        }
                    });

                    return entry;
                });

                this.entries = entries;
                resolve(entries);
            });
        });
    }

    lookupCountryCode(countryID) {
        var country = _.find(this.countryCodes, { 'ID': countryID });
        return country ? country.ISO : null;
    }

    getSubmissionRange() {
        return new Promise((resolve, reject) => {
            if (this.submissionDates) {
                resolve(this.submissionDates);
            }

            this.getData().then((entries) => {
                var dates = _.countBy(entries, (entry) => {
                    let date = new Date(entry.submitted);
                    date.setHours(0,0,0,0);
                    return date.toISOString();
                });

                this.submissionDates = _.map(dates, (value, key) => {
                    let date = new Date(key),
                        label = date.getDate() + '/' + (date.getMonth() + 1)
                    return {
                        label: label,
                        date: date.getTime(),
                        count: value
                    };
                });

                resolve(this.submissionDates);
            });
        });
    }

    getParticipatingCountries(dateRange) {
        return new Promise((resolve, reject) => {
            this.getData().then((entries) => {
                if (dateRange) {
                    entries = _.filter(entries, (entry) => {
                        let date = new Date(entry.submitted);
                        date.setHours(0,0,0,0);
                        date = date.getTime();

                        if (!dateRange) {
                            return true;
                        } else if (dateRange.length == 1) {
                            return (date == dateRange[0]);
                        } else if (dateRange.length == 2) {
                            return (date >= dateRange[0] && date <= dateRange[1]);
                        }
                    });
                }

                var countryData = _.countBy(entries, 'country');
                resolve(countryData);
            });
        });
    }

    getEntriesByCountry(countryISO) {
        return new Promise((resolve, reject) => {
            this.getData().then((entries) => {
                entries = _.filter(entries, (entry) => entry.country === countryISO);

                resolve(entries);
            });
        });
    }

}

export default hackerRankDataService;